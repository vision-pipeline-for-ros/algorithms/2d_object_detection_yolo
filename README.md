![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# 2D single object detection (Yolo)

This is an implementation of a object detection with [yolo](https://pjreddie.com/media/files/papers/YOLOv3.pdf).  
There are several setting options.  

Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/algorithms/2d_object_detection_yolo/-/wikis/home)
