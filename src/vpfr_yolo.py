"""
2d_object_detection_yolo
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

from vpfr2dobjectdetectionclutteredscene.msg import *
import rospy
from cv_bridge import CvBridge, CvBridgeError
import cv2


from geometry_msgs.msg import Pose, Point, Quaternion
from algorithm_template import AlgorithmTemplate
from sensor_msgs.msg import CameraInfo, Image
import numpy as np
import tf
import os
import rospkg


class VpfrYolo(AlgorithmTemplate):
    def __init__(self):
        self.__imagepub = None

    def get_io_type(self):
        return Image, object2DArray

    def on_enable(self):
        rospy.loginfo("[algorithm][object_2d_detection_yolo] init")
        self.__bridge = CvBridge()
        self.__blob_size_w = 416
        self.__blob_size_h = 416
        self.__blob_scale = 1 / 255.0
        self.__net = None
        self.__use_gpu = "opencl"
        rospy.loginfo("[algorithm][object_2d_detection_yolo] end")

    def on_disable(self):
        pass

    def main(self, req):
        if self.__net is not None:
            cv_image = self.__bridge.imgmsg_to_cv2(req)

            blob = cv2.dnn.blobFromImage(
                cv_image,
                self.__blob_scale,
                (self.__blob_size_h, self.__blob_size_w),
                swapRB=True,
                crop=False,
            )

            self.__net.setInput(blob)
            layer_outputs = self.__net.forward(self.__layer_name)

            boxes = []
            confidences = []
            class_ids = []
            h, w = cv_image.shape[:2]

            for layer_output in layer_outputs:
                for detection in layer_output:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > 0.5:
                        box = detection[:4] * np.array([w, h, w, h])
                        (center_x, center_y, width, height) = box.astype("int")
                        x = int(center_x - (width / 2))
                        y = int(center_y - (height / 2))
                        box = [x, y, int(width), int(height)]
                        boxes.append(box)
                        confidences.append(float(confidence))
                        class_ids.append(class_id)

            listoutput = []
            indices = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
            if len(indices) > 0:
                for i in indices.flatten():
                    (x, y) = (boxes[i][0], boxes[i][1])
                    (w, h) = (boxes[i][2], boxes[i][3])
                    listoutput.append(
                        object2D(
                            x, y, h, w, self.__classes[class_ids[i]], confidences[i]
                        )
                    )
                    if self.__publish_image and self.__imagepub is not None:
                        color = [int(c) for c in self.__colors[class_ids[i]]]
                        text = "{}: {:.4f}".format(
                            self.__classes[class_ids[i]], confidences[i]
                        )
                        cv2.rectangle(cv_image, (x, y), (x + w, y + h), color, 2)
                        cv2.putText(
                            cv_image,
                            text,
                            (x, y - 5),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            0.5,
                            color,
                            1,
                        )
            if self.__publish_image and self.__imagepub is not None:
                ros_image = self.__bridge.cv2_to_imgmsg(cv_image, "rgb8")
                self.__imagepub.publish(ros_image)

            return object2DArray(listoutput)
        else:
            return object2DArray(None)

    def on_additional_data_change(self, data):
        if len(data) >= 1:
            self.__image_output_topic = data[0]
            if self.__imagepub is not None:
                self.__imagepub.unregister()
            self.__imagepub = rospy.Publisher(
                self.__image_output_topic, Image, queue_size=10
            )
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] write image to '"
                + self.__image_output_topic
                + "'"
            )
        else:
            rospy.loginfo("[algorithm][object_2d_detection_yolo] no additional data")

    def on_config_change(self, item):
        if item is None:
            return

        yolo_config_path = item.get("yolo_config_path")
        if yolo_config_path is not None:
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] yolo_config_path set to '%s'",
                yolo_config_path,
            )
            self.__config_path = os.path.expanduser(yolo_config_path)
        else:
            rospy.logwarn(
                "[algorithm][object_2d_detection_yolo] yolo_config_path file is not set"
            )

        names = item.get("yolo_class_names_path")
        if names is not None:
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] yolo_class_names_path set to '%s'",
                names,
            )
            names = os.path.expanduser(names)
            self.__classes = open(names).read().strip().split("\n")
        else:
            rospy.logwarn(
                "[algorithm][object_2d_detection_yolo] yolo_class_names_path file is not set"
            )

        yolo_weights_path = item.get("yolo_weights_path")
        if yolo_weights_path is not None:
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] yolo_weights_path set to '%s'",
                yolo_weights_path,
            )
            self.__weights_path = os.path.expanduser(yolo_weights_path)
        else:
            rospy.logwarn(
                "[algorithm][object_2d_detection_yolo] yolo_weights_path file is not set"
            )

        publish_image = item.get("publish_image")
        if publish_image is not None:
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] publish_image set to '%s'",
                publish_image,
            )
            self.__publish_image = publish_image
        else:
            rospy.logwarn(
                "[algorithm][object_2d_detection_yolo] publish_image is not set"
            )

        blob_size_w = item.get("blob_size_w")
        if blob_size_w is not None:
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] blob_size_w set to '%s'",
                blob_size_w,
            )
            self.__blob_size_w = blob_size_w
        else:
            rospy.logwarn(
                "[algorithm][object_2d_detection_yolo] blob_size_w is not set"
            )

        blob_size_h = item.get("blob_size_h")
        if blob_size_h is not None:
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] blob_size_h set to '%s'",
                blob_size_h,
            )
            self.__blob_size_h = blob_size_h
        else:
            rospy.logwarn(
                "[algorithm][object_2d_detection_yolo] blob_size_h is not set"
            )

        blob_scale = item.get("blob_scale")
        if blob_scale is not None:
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] blob_scale set to '%s'",
                blob_scale,
            )
            self.__blob_scale = blob_scale
        else:
            rospy.logwarn("[algorithm][object_2d_detection_yolo] blob_scale is not set")

        use_gpu = item.get("use_gpu")
        if use_gpu is not None:
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] use_gpu set to '%s'", use_gpu
            )
            self.__use_gpu = use_gpu
        else:
            rospy.logwarn("[algorithm][object_2d_detection_yolo] use_gpu is not set")

        self.__colors = np.random.randint(
            0, 255, size=(len(self.__classes), 3), dtype="uint8"
        )
        self.__net = cv2.dnn.readNetFromDarknet(self.__config_path, self.__weights_path)
        if self.__use_gpu == "cuda":
            rospy.loginfo("[algorithm][object_2d_detection_yolo] use cuda for backend")
            self.__net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
            self.__net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
        elif self.__use_gpu == "opencl":
            rospy.loginfo(
                "[algorithm][object_2d_detection_yolo] use opencl for backend"
            )
            self.__net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
            self.__net.setPreferableTarget(cv2.dnn.DNN_TARGET_OPENCL_FP16)
        else:
            rospy.loginfo("[algorithm][object_2d_detection_yolo] use cpu for backend")
            self.__net.setPreferableBackend(cv2.dnn.DNN_BACKEND_DEFAULT)
            self.__net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

        self.__layer_name = self.__net.getLayerNames()
        self.__layer_name = [
            self.__layer_name[i[0] - 1] for i in self.__net.getUnconnectedOutLayers()
        ]
